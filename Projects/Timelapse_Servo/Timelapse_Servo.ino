/**
 * Timelapse Servo
 * 
 * panoraming 180 degrees slowly for use with GoPro
 */
#include <Servo.h>
Servo servo;

int servoPin = 8;
int angle = 0;

bool asc = true;

void setup() {
  servo.attach(servoPin);
}

void loop() {
  delay(2000);
  servo.write(angle);

  if (angle > 180) {
    asc = false;
  } else if (angle < 0) {
    asc = true;
  }

  if (asc) {
    angle++;
  } else {
    angle--;
  }
}
