/**
 * Servo laser
 * 
 * A laser mounted on top of a servo
 */
#include <Servo.h>
Servo servo;

const int servoPin = 8;
const int laserPin = 9;

int angle = 0;

void setup() {
  servo.attach(servoPin);
  pinMode(laserPin, OUTPUT);  
  digitalWrite(laserPin, HIGH);
}

void loop() {
  delay(50);
  servo.write(angle);

  if (angle > 180) {
    angle = 0;
  }

  angle ++;
}
