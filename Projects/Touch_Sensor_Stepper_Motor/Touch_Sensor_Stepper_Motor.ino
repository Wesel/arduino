/**
 * Touch Sensor controlling stepper
 */
#include <CheapStepper.h>

CheapStepper stepper;
const int sensorPin = 7;
const int OFF = 0;
const int ON = 1;
 
void setup() {
  pinMode(sensorPin, INPUT);
}

void loop() {
  int reading = digitalRead(sensorPin);
  
  if (reading == OFF) {
    bool moveClockwise = false;
    int stepsToMove = 1;
    stepper.move(moveClockwise, stepsToMove);
    delay(10);
  }

  if (reading == ON) {
    keepMoving();
  }
}

void keepMoving() {
   bool moveClockwise = true;
   int stepsToMove = 100;
   
   stepper.move(moveClockwise, stepsToMove);
}
