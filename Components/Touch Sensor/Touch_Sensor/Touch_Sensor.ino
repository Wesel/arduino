/**
 * Touch Sensor
 */
const int sensorPin = 8;

const int OFF = 0;
const int ON = 1;
 
void setup() {
  pinMode(sensorPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  int reading = digitalRead(sensorPin);
  
  if (reading == OFF) {
    Serial.println("No touch");
  }

  if (reading == ON) {
    Serial.println("Touch");
  }
  
  delay(100);
}
