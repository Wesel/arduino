/**
 * 4 digit display
 * 
 * Display: 3642BH
 * 
 * Use TM1637-master.zip as library
 *      
 *      A
 *     ---
 *   F | | B
 *     -G- 
 *   E | | C
 *     --- 
 *      D
 */   
 
#include <TM1637Display.h>


#define CLK 2
#define DIO 3

#define TEST_DELAY   200

TM1637Display display(CLK, DIO);

const uint8_t SEG_DONE[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
};

const uint8_t SEG_AEW[] = {
  SEG_A | SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,  // A
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G,          // E
  SEG_D | SEG_E | SEG_F | SEG_C | SEG_B,          // V
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_F           // V
};


const uint8_t SEG_LETTER_A[] = { SEG_A | SEG_B | SEG_C | SEG_E | SEG_F | SEG_G };
const uint8_t SEG_LETTER_B[] = { SEG_F | SEG_E | SEG_D | SEG_C | SEG_G };
const uint8_t SEG_LETTER_C[] = { SEG_A | SEG_F | SEG_E | SEG_D };
const uint8_t SEG_LETTER_D[] = { SEG_B | SEG_C | SEG_D | SEG_E | SEG_G };
const uint8_t SEG_LETTER_E[] = { SEG_A | SEG_D | SEG_E | SEG_F | SEG_G };
const uint8_t SEG_LETTER_W[] = { SEG_D | SEG_E | SEG_F | SEG_C, SEG_B | SEG_C | SEG_D | SEG_E };

  
void setup() {
  //display.setBrightness(1);           // 1 - 7
  display.setBrightness(0x0f, true);  // ON
  //display.setBrightness(0x0f, false); // OFF
}

void loop() {

  // Letters
  display.setSegments(SEG_AEW);      // AEW
  delay(1000);
 
  // Tal
  display.showNumberDec(10, true);  // 0010
  delay(1000);
  display.showNumberDec(10, false); // 10
  delay(1000);
 
  /** Run through all the dots
  int k;
  for(k = 0; k <= 4; k++) {
    display.showNumberDecEx(0, (0x80 >> k), true);
    delay(200);
  }
  **/
}















