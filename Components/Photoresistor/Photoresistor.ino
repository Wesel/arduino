/**
 * Photoresistor
 */
const int sensorPin = 8;

const int onValue = 0;
const int offValue = 1;

void setup() {
  pinMode(sensorPin, INPUT);
}

void loop() {
  int sensorValue = digitalRead(sensorPin);

  if (sensorValue == onValue) {
    // Light is ON
  }

  if (sensorValue == offValue) {
    // Light is OFF
  } 

  delay(200);
}
