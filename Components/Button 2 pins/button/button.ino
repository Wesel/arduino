/**
 * Button with 2 pins
 * 
 * The readings are very unstable, a lot of readings 
 * is needed to make sure it's an actual press
 * 
 * 0: button pressed
 * 1: button not pressed
 */
const int buttonPin = 8;

const int numberOfSamples = 25;
const int delayBetweenSamples = 1;
const int delayButtonRepeat = 150;

int count = 0;

void setup() {
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  if (buttonPressed(buttonPin)) {
    count++;
    Serial.println(count);
    delay(delayButtonRepeat);
  }
}

/**
 * Button pressed 
 * 
 * (remember const from the top)
 * 
 * @param int buttonPin  
 * @return bool
 */
bool buttonPressed(int buttonPin) {
  int sampleResults = 0;

  // Collect samples to make sure the button is pressed
  for (int i = 0; i < numberOfSamples; i++) {
    sampleResults += digitalRead(buttonPin);
    delay(delayBetweenSamples);
  }
 
  if (sampleResults == 0) {
    return true;
  }

  return false;
}













