/**
 * Joystick
 */

// Analog
const int xPin = 0;
const int yPin = 1;
const int switchPin = 13;

const int OFF = 0;
const int ON = 1;

void setup() {
  Serial.begin(9600);
}

void loop() {
  int x = analogRead(xPin);
  int y = analogRead(yPin);
  int switchValue = digitalRead(switchPin); 

  // Serial.println(switchValue);
  
  //Serial.print("X: ");
  //Serial.println(x);

  //Serial.print("Y: ");
  //Serial.println(y);

  delay(1000);
}
