/**
 * Servo
 */
#include <Servo.h>

const int servoPin = 8;
int angle = 0;
Servo servo;

void setup() {
  servo.attach(servoPin);
}

void loop() {
  delay(50);
  servo.write(angle);

  if (angle > 180) {
    angle = 0;
  }

  angle ++;
}
