/**
 * Stepper motor
 *
 * Stepper Motor BYJ48 5v
 * Driver Board ULN2003
 * 
 * Use CheapStepper-master.zip as library
 * 
 * Max step: 4096
 */

#include <CheapStepper.h>

CheapStepper stepper;
// here we declare our stepper using default pins:
// 8  <--> IN1
// 9  <--> IN2
// 10 <--> IN3
// 11 <--> IN4

void setup() { 
  
}

void loop() {
  // keepMoving();
   moveDegrees(90);
  // moveTo(2000);
 
  delay(500);
}

void keepMoving() {
   bool moveClockwise = true;
   int stepsToMove = 3;
   
   stepper.move(moveClockwise, stepsToMove);
}

void moveDegrees(int degrees) {
  bool moveClockwise = true;
  stepper.moveDegrees(moveClockwise, degrees);
}

void moveTo(int step) {
  bool moveClockwise = true;
  stepper.moveTo(moveClockwise, step);
}
