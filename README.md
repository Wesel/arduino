# Arduino documentation

## Folder structure:   
 - Projects
 - Components
 - Software components

### Projects
Folders with different projects for arduino.   
 
 - No projects yet
 - No projects yet

### Components
Documentation for each component for easier user later.   
 
 - 4 Digit Display
 - Button 2 pins
 - DC Motor 
 - Laser
 - LED
 - Light Sensor 
 - Servo
 - Ultrasound Sensor

### Software components
Arduino software documentation for cool functions and more.   
 
 - No components yet
 - No components yet

## Ideas
#### Laser communication 
Use one arduino to send data via laser and another to read via the photocell.

#### Radio communication 
You can get a sender and receiver module and it would be fun to have two arduino's communicating.
